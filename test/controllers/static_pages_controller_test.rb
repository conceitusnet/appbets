require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  
  test "should get home" do
    get root_url
    assert_response :success
    assert_select "title", "Bolão do brasileirão 2017"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Ajuda | Bolão do brasileirão 2017"
  end

  test "should get about" do
  	get about_path
  	assert_response :success
    assert_select "title", "Regras | Bolão do brasileirão 2017"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contatos | Bolão do brasileirão 2017"
  end
end
