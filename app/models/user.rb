class User < ApplicationRecord
	attr_accessor :remember_token
	before_save { self.email = email.downcase }
	validates :name,  presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 },
					  format: { with: VALID_EMAIL_REGEX },
					  uniqueness: { case_sensitive: false }

	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

	# Retorna o digest de hash da string dada
	def self.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
	end

	# Retorna um token aleatório
	def self.new_token
		SecureRandom.urlsafe_base64
	end

	# Retorna o digest de hash da string dada
	def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

	# Retorna um token aleatório
	def User.new_token
		SecureRandom.urlsafe_base64
	end

	# Lembra um usuário no banco de dados para uso em sessões persistentes.
	def remember
		self.remember_token = User.new_token
		update_attribute(:remember_digest, User.digest(remember_token))
	end

	 # Retorna true se o token fornecido corresponder ao digest
	 def authenticated?(remember_token)
		 return false if remember_digest.nil?
		 BCrypt::Password.new(remember_digest).is_password?(remember_token)
	 end

	 # Esquecendo um usuário
	 def forget
	 	update_attribute(:remember_digest, nil)
	 end
end
